# Rock Paper Scissors Game

Rock paper scissors is a hand game usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand. These shapes are "rock" (a closed fist), "paper" (a flat hand), and "scissors" (a fist with the index finger and middle finger extended, forming a V). "Scissors" is identical to the two-fingered V sign (also indicating "victory" or "peace") except that it is pointed horizontally instead of being held upright in the air.

## System Requirements 

- Java 11
- Maven [mvn](https://maven.apache.org/)

## Run application

```bash
 mvn spring-boot:run
```

## Usage
We can see the available services from [Swagger documentation](http://localhost:8080/swagger-ui/index.html).

1. Firstly, we need to create a player to start the game:
```bash
curl --location --request POST 'http://localhost:8080/player/signup' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userName": "agustin1@email.com",
    "password": "pass"
}'
```
2. Once we have already our player, we must login into the system:
```bash
curl --location --request POST 'http://localhost:8080/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userName": "agustin1@email.com",
    "password": "pass"
}'
```
3. If you have been authenticated rightly, the application will return the token into the header to make requests:

Authorization = `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9...`

4. Finally, you can start with the game!
```bash
curl --location --request POST 'http://localhost:8080/game/play' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9...' \
--header 'Content-Type: application/json' \
--data-raw '{
    "userName": "agustin1@email.com"
}'
```
Getting the following information:
```json
{
    "gameId": "61b70f00-1dbb-4e29-af9f-b4e5530819f9",
    "playerOne": "agustin1@email.com",
    "playerTwo": "elon.musk@tesla.com",
    "count": 1,
    "rounds": [
        {
            "playerOneChoice": "SCISSOR",
            "playerTwoChoice": "ROCK",
            "result": "PLAYER2_WINS"
        }
    ]
}
```
5. Restart the game:
```bash
curl --location --request PUT 'http://localhost:8080/game/restart' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ...' \
--header 'Content-Type: application/json' \

--data-raw '{
    "email": "agustin1@email.com"
}'
```
6. Check general information of the game:
```bash
curl --location --request GET 'http://localhost:8080/game/info' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJz...'
```
Getting the following information:
```json
{
    "totalPlayerOneWins": 0,
    "totalPlayerTwoWins": 2,
    "totalDraws": 4,
    "totalRounds": 6
}
```

## License 
[Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)
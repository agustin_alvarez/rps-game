package com.ciklum.alvarez;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ciklum.alvarez.common.pojo.GameSingletonBean;
import com.ciklum.alvarez.common.pojo.PlayerSingletonBean;

@SpringBootApplication
public class RockPaperScissorsApplication {

    public static void main(final String[] args) {
	SpringApplication.run(RockPaperScissorsApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
	return new BCryptPasswordEncoder();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public GameSingletonBean gameSingletonBean() {
	return GameSingletonBean.builder().build();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public PlayerSingletonBean playerSingletonBean() {
	return PlayerSingletonBean.builder().build();
    }

}

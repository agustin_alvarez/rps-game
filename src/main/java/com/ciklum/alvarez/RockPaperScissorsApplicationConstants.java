package com.ciklum.alvarez;

/**
 *
 * @author agustin.alvarez
 *
 */
public final class RockPaperScissorsApplicationConstants {

    private RockPaperScissorsApplicationConstants() {
	throw new IllegalStateException("GameApplicationConstants class");
    }

    public static final String SECRET = "IAmTheSecretKeywToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/player/signup";

}

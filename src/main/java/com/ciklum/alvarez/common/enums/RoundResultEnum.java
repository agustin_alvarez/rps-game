package com.ciklum.alvarez.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RoundResultEnum {

    PLAYER1_WINS(1), PLAYER2_WINS(-1), DRAW(0);

    private final int value;
}
package com.ciklum.alvarez.common.enums;

import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TypeEnum {
    ROCK(0), PAPER(1), SCISSOR(2);

    private final int value;

    public static TypeEnum parse(final String value) {
	return Stream.of(TypeEnum.values()) //
		.filter(o -> o.name().equalsIgnoreCase(value)) //
		.findFirst() //
		.orElse(null);
    }

    public static TypeEnum parse(final int value) {
	return Stream.of(TypeEnum.values()) //
		.filter(o -> o.value == value) //
		.findFirst() //
		.orElse(null);
    }

    public static RoundResultEnum wins(final TypeEnum playerOne, final TypeEnum playerTwo) {
	if (playerOne != playerTwo) {
	    switch (playerOne) {
	    case ROCK:
		return playerTwo == SCISSOR ? RoundResultEnum.PLAYER1_WINS : RoundResultEnum.PLAYER2_WINS;
	    case PAPER:
		return playerTwo == ROCK ? RoundResultEnum.PLAYER1_WINS : RoundResultEnum.PLAYER2_WINS;
	    case SCISSOR:
		return playerTwo == PAPER ? RoundResultEnum.PLAYER1_WINS : RoundResultEnum.PLAYER2_WINS;
	    default:
		break;
	    }
	}

	return RoundResultEnum.DRAW;

    }
}

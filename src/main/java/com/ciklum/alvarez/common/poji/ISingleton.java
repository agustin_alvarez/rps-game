package com.ciklum.alvarez.common.poji;

import java.util.List;
import java.util.Optional;

import com.ciklum.alvarez.common.pojo.GameWsApiException;

public interface ISingleton<K, V> {

    public void insert(final V value) throws GameWsApiException;

    public Optional<V> get(final K key) throws GameWsApiException;

    public List<V> getValues() throws GameWsApiException;

}

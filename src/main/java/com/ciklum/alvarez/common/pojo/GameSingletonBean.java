package com.ciklum.alvarez.common.pojo;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.ciklum.alvarez.common.poji.ISingleton;
import com.ciklum.alvarez.model.entity.GameEntity;

import lombok.Builder;
import lombok.Getter;

@Builder
public class GameSingletonBean implements ISingleton<UUID, GameEntity> {

    @Builder.Default
    private final Map<UUID, GameEntity> gameInMemoryMap = new ConcurrentHashMap<>();
    
    @Getter
    @Builder.Default
    private Long totalPlayerOneWins = 0L;
    
    @Getter
    @Builder.Default
    private Long totalPlayerTwoWins = 0L;
    
    @Getter
    @Builder.Default
    private Long totalDraws = 0L;
    
    public Long getTotalRounds() {
	return totalPlayerOneWins + totalPlayerTwoWins + totalDraws;
    }
    
    public synchronized void insert(final GameEntity gameEntity) throws GameWsApiException {
	try {
	    this.gameInMemoryMap.put(gameEntity.getGameId(), gameEntity);
	} catch (Exception e) {
	    throw GameWsApiException.builder().defaultMessage("Couldn't create the game").statusCode(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
    }
    
    public synchronized void incrementTotalPlayerOneWins() {
	totalPlayerOneWins++;
    }
    
    public synchronized void incrementTotalPlayerTwoWins() {
	totalPlayerTwoWins++;
    }
    
    public synchronized void incrementTotalDraws() {
	totalDraws++;
    }

    public Optional<GameEntity> get(final UUID key) throws GameWsApiException {
	try {
	    return Optional.ofNullable(gameInMemoryMap.get(key));
	} catch (Exception e) {
	    throw GameWsApiException.builder().defaultMessage("Couldn't get the game").statusCode(HttpStatus.NOT_FOUND).build();
	}
    }

    public List<GameEntity> getValues() throws GameWsApiException {
	try {
	    return this.gameInMemoryMap.values()//
		    .stream()//
		    .collect(Collectors.toUnmodifiableList());
	} catch (Exception e) {
	    throw GameWsApiException.builder().defaultMessage("Couldn't get the game").statusCode(HttpStatus.NOT_FOUND).build();
	}

    }

}

package com.ciklum.alvarez.common.pojo;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GameWsApiException extends Exception {

    /**
     * Generated serial version uuid.
     */
    private static final long serialVersionUID = 3079822438844360766L;

    /**
     * The error key (i18n).
     */
    private final HttpStatus statusCode;

    /**
     * A default message (useful for logging or if no i18n is available).
     */
    private final String defaultMessage;
}

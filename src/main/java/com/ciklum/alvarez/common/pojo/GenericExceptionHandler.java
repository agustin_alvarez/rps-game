package com.ciklum.alvarez.common.pojo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author agustin.alvarez
 *
 */
@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(GameWsApiException.class)
    public ResponseEntity<ErrorMessage> handleGameWsApiException(final GameWsApiException ex) {
	this.logger.error(ex.getMessage(), ex);

	return ResponseEntity.status(ex.getStatusCode())//
		.body(ErrorMessage.builder()//
			.status(ex.getStatusCode().value())//
			.message(ex.getDefaultMessage())//
			.build());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> unhandledExceptions(final Exception ex) {
	this.logger.error(ex.getMessage(), ex);

	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)//
		.body(ErrorMessage.builder()//
			.status(HttpStatus.INTERNAL_SERVER_ERROR.value())//
			.message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())//
			.build());
    }

}
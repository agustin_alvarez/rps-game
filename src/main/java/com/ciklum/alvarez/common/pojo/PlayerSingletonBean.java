package com.ciklum.alvarez.common.pojo;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;

import com.ciklum.alvarez.common.poji.ISingleton;
import com.ciklum.alvarez.model.entity.PlayerEntity;

import lombok.Builder;

@Builder
public class PlayerSingletonBean implements ISingleton<String, PlayerEntity> {

    private static final String MSG_ERROR_PLAYERS_NOT_FOUND = "Couldn't find the players";
    private static final String MSG_ERROR_PLAYER_NOT_FOUND = "Couldn't find the player";
    private static final String MSG_ERROR_PLAYER_NOT_CREATED = "Couldn't create the player";
    @Builder.Default
    private final Map<String, PlayerEntity> playersInMemory = new ConcurrentHashMap<>();

    @Override
    public synchronized void insert(final PlayerEntity player) throws GameWsApiException {
	try {
	    this.playersInMemory.put(player.getUserName(), player);
	} catch (final Exception e) {
	    throw GameWsApiException.builder().defaultMessage(MSG_ERROR_PLAYER_NOT_CREATED).statusCode(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

    }

    @Override
    public Optional<PlayerEntity> get(final String key) throws GameWsApiException {
	try {
	    return Optional.ofNullable(playersInMemory.get(key));
	} catch (final Exception e) {
	    throw GameWsApiException.builder().defaultMessage(MSG_ERROR_PLAYER_NOT_FOUND).statusCode(HttpStatus.NOT_FOUND).build();
	}
    }

    @Override
    public List<PlayerEntity> getValues() throws GameWsApiException {
	try {
	    return this.playersInMemory.values()//
		    .stream()//
		    .collect(Collectors.toUnmodifiableList());
	} catch (final Exception e) {
	    throw GameWsApiException.builder().defaultMessage(MSG_ERROR_PLAYERS_NOT_FOUND).statusCode(HttpStatus.NOT_FOUND).build();
	}
    }

}

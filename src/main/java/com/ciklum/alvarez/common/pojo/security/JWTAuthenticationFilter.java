package com.ciklum.alvarez.common.pojo.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.ciklum.alvarez.RockPaperScissorsApplicationConstants;
import com.ciklum.alvarez.service.pojo.dto.PlayerRequestJsonDto;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(final AuthenticationManager authenticationManager) {
	this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest req, final HttpServletResponse res) throws AuthenticationException {
	try {
	    final PlayerRequestJsonDto creds = new ObjectMapper().readValue(req.getInputStream(), PlayerRequestJsonDto.class);

	    return this.authenticationManager.authenticate(//
		    new UsernamePasswordAuthenticationToken(creds.getUserName(), creds.getPassword(), new ArrayList<>()));
	} catch (final IOException e) {
	    throw new RuntimeException(e);
	}
    }

    @Override
    protected void successfulAuthentication(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain, final Authentication auth) throws IOException, ServletException {

	final String token = JWT.create().withSubject(((User) auth.getPrincipal()).getUsername())
		.withExpiresAt(new Date(System.currentTimeMillis() + RockPaperScissorsApplicationConstants.EXPIRATION_TIME))
		.sign(Algorithm.HMAC512(RockPaperScissorsApplicationConstants.SECRET.getBytes()));

	res.addHeader(RockPaperScissorsApplicationConstants.HEADER_STRING, RockPaperScissorsApplicationConstants.TOKEN_PREFIX + token);
    }

}
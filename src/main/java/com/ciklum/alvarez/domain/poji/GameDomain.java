package com.ciklum.alvarez.domain.poji;

import java.util.List;
import java.util.Optional;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;

public interface GameDomain {

    GameTotalInfoDto findGameTotalInfo();

    Optional<GameDto> findGameById(String gameId) throws GameWsApiException;

    List<GameDto> findAllGames() throws GameWsApiException;

    /**
     *
     * @param playerId
     * @return
     * @throws GameWsApiException
     */
    GameDto createGame(String playerId) throws GameWsApiException;

    /**
     *
     * @param playerId
     * @return
     * @throws GameWsApiException
     */
    GameDto restart(String playerId) throws GameWsApiException;

    /**
     *
     * @param playerId
     * @param gameId
     * @throws GameWsApiException
     */
    GameDto play(PlayRequestJsonDto playRequestDto) throws GameWsApiException;

}
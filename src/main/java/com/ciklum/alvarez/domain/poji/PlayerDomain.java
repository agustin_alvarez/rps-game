package com.ciklum.alvarez.domain.poji;

import java.util.List;
import java.util.Optional;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;

public interface PlayerDomain {

    /**
     *
     * @param playerId
     * @return
     * @throws GameWsApiException
     */
    public Optional<PlayerDto> findPlayerById(String playerId) throws GameWsApiException;

    /**
     *
     * @return
     * @throws GameWsApiException
     */
    public List<PlayerDto> findAllPlayers() throws GameWsApiException;

    /**
     *
     * @param playerId
     * @return
     * @throws GameWsApiException
     */
    public void createPlayer(PlayerDto playerDto) throws GameWsApiException;

}

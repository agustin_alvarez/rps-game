package com.ciklum.alvarez.domain.pojo;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ciklum.alvarez.common.enums.TypeEnum;
import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.poji.GameDomain;
import com.ciklum.alvarez.domain.poji.PlayerDomain;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.persistence.poji.GamePersistence;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;

/**
 *
 * @author agustin.alvarez
 *
 */
@Service
public class GameDomainImpl implements GameDomain {

    private final Logger logger = LoggerFactory.getLogger(GameDomainImpl.class);

    @Autowired
    private GamePersistence gamePersistence;

    @Autowired
    private PlayerDomain playerDomain;

    private static final int MIN = 0;
    private static final int MAX = 2;

    @Override
    public GameTotalInfoDto findGameTotalInfo() {
	return this.gamePersistence.findGameTotalInfo();
    }

    @Override
    public Optional<GameDto> findGameById(final String gameId) throws GameWsApiException {
	return this.gamePersistence.findGameByUUID(UUID.fromString(gameId));
    }

    @Override
    public List<GameDto> findAllGames() throws GameWsApiException {
	return this.gamePersistence.findAllGames();
    }

    @Override
    public GameDto createGame(final String playerId) throws GameWsApiException {

	this.logger.debug("Creating Game...");

	final Optional<PlayerDto> playerDto = this.playerDomain.findPlayerById(playerId);

	if (playerDto.isPresent()) {
	    return this.gamePersistence.createGame(playerDto.get());
	}

	throw GameWsApiException.builder().defaultMessage("Couldn't create the game").statusCode(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @Override
    public GameDto restart(final String playerId) throws GameWsApiException {

	this.logger.debug("Restarting Game...");

	final Optional<PlayerDto> playerDto = this.playerDomain.findPlayerById(playerId);

	if (playerDto.isPresent()) {
	    return this.gamePersistence.restart(playerDto.get());
	}

	throw GameWsApiException.builder().defaultMessage("Couldn't restart the game").statusCode(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @Override
    public GameDto play(final PlayRequestJsonDto playRequestDto) throws GameWsApiException {
	Optional<GameDto> currentGame = this.gamePersistence.findGameByPlayerEmail(playRequestDto.getUserName());

	if (!currentGame.isPresent()) {
	    currentGame = Optional.of(this.createGame(playRequestDto.getUserName()));
	}

	this.playGame(currentGame.get());

	return this.gamePersistence.findGameByUUID(currentGame.get().getGameId())//
		.orElse(null);
    }

    // ~ Private methods ======================================================

    private RoundDto playGame(final GameDto game) throws GameWsApiException {

	final TypeEnum beatPlayerOne = this.beatPlayerOne();
	final TypeEnum beatPlayerTwo = TypeEnum.ROCK;

	final RoundDto roundDto = RoundDto.builder()//
		.playerOneChoice(beatPlayerOne)//
		.playerTwoChoice(beatPlayerTwo)//
		.result(TypeEnum.wins(beatPlayerOne, beatPlayerTwo))//
		.build();

	this.gamePersistence.insertRound(game, roundDto);

	return roundDto;
    }

    private TypeEnum beatPlayerOne() {
	return TypeEnum.parse(new Random().nextInt((MAX + 1) - MIN) + MIN);
    }

}

package com.ciklum.alvarez.domain.pojo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.poji.PlayerDomain;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.persistence.poji.PlayerPersistence;

@Service
public class PlayerDomainImpl implements PlayerDomain, UserDetailsService {

    private static final String MSG_ERROR_USERNAME_IS_EMPTY = "Username is empty";
    private static final String MSG_ERROR_PLAYER_ALREADY_EXISTS = "Player already exists.";
    private static final String MSG_ERROR_USERNAME_NOT_EXIST = "Username doesn't exist";
    @Autowired
    private PlayerPersistence playerPersistence;

    @Override
    public Optional<PlayerDto> findPlayerById(final String playerId) throws GameWsApiException {
	return this.playerPersistence.findPlayerById(playerId);
    }

    @Override
    public List<PlayerDto> findAllPlayers() throws GameWsApiException {
	return this.playerPersistence.findAllPlayers();
    }

    @Override
    public void createPlayer(final PlayerDto playerDto) throws GameWsApiException {
	this.validatePlayer(playerDto);

	this.playerPersistence.createPlayer(playerDto);
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
	try {
	    return this.playerPersistence.findPlayerById(username)//
		    .map(this::buildUser)//
		    .orElseThrow(() -> GameWsApiException.builder().defaultMessage(MSG_ERROR_USERNAME_NOT_EXIST).statusCode(HttpStatus.BAD_REQUEST).build());
	} catch (final GameWsApiException e) {
	    throw new UsernameNotFoundException(username);
	}
    }

    // ~ Private methods ===========================================

    private User buildUser(final PlayerDto user) {
	return new User(user.getUserName(), user.getPassword(), Collections.emptyList());
    }

    private void validatePlayer(final PlayerDto playerDto) throws GameWsApiException {
	Optional.ofNullable(playerDto)//
		.map(PlayerDto::getUserName)//
		.orElseThrow(() -> GameWsApiException.builder().defaultMessage(MSG_ERROR_USERNAME_IS_EMPTY).statusCode(HttpStatus.BAD_REQUEST).build());

	if (this.playerPersistence.findPlayerById(playerDto.getUserName()).isPresent()) {
	    throw GameWsApiException.builder().defaultMessage(MSG_ERROR_PLAYER_ALREADY_EXISTS).statusCode(HttpStatus.BAD_REQUEST).build();
	}
    }

}

package com.ciklum.alvarez.domain.pojo.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@EqualsAndHashCode
@ToString
public class GameDto {
    
    private final UUID gameId;
    private final PlayerDto playerOne;
    private final PlayerDto playerTwo; 
    private final String createdBy;
    private final LocalDateTime createdAt;
    private final Long count;
    private final List<RoundDto> rounds;
}


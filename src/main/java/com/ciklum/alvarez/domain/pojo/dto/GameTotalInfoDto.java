package com.ciklum.alvarez.domain.pojo.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GameTotalInfoDto {

    private final Long totalPlayerOneWins;
    private final Long totalPlayerTwoWins;
    private final Long totalDraws;
    private final Long totalRounds;

}

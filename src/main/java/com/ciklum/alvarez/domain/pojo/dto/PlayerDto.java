package com.ciklum.alvarez.domain.pojo.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@EqualsAndHashCode
@ToString
public class PlayerDto {

    private final String userName;
    private final String password;
}

package com.ciklum.alvarez.domain.pojo.dto;

import java.util.UUID;

import com.ciklum.alvarez.common.enums.RoundResultEnum;
import com.ciklum.alvarez.common.enums.TypeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RoundDto {

    private UUID roundId;
    private TypeEnum playerOneChoice;
    private TypeEnum playerTwoChoice;
    private RoundResultEnum result;

}

package com.ciklum.alvarez.model.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class GameEntity {
    
    private UUID gameId;
    private PlayerEntity playerOne;
    private PlayerEntity playerTwo;
    
    @Builder.Default
    private List<RoundEntity> rounds = new ArrayList<>();
    
    private String createdBy;
    private LocalDateTime createdAt;
    
    @Builder.Default
    private Long count = 0L;
}


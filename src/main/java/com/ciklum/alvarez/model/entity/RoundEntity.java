package com.ciklum.alvarez.model.entity;

import java.util.UUID;

import com.ciklum.alvarez.common.enums.RoundResultEnum;
import com.ciklum.alvarez.common.enums.TypeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RoundEntity {

    private UUID roundId;
    private TypeEnum playerOneChoice;
    private TypeEnum playerTwoChoice;
    private RoundResultEnum result;

}

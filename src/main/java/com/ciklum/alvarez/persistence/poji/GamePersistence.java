package com.ciklum.alvarez.persistence.poji;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;

public interface GamePersistence {

    public GameDto createGame(PlayerDto player) throws GameWsApiException;

    public Optional<GameDto> findGameByUUID(final UUID gameId) throws GameWsApiException;

    public Optional<GameDto> findGameByPlayerEmail(final String email) throws GameWsApiException;

    public List<GameDto> findAllGames() throws GameWsApiException;

    public void insertRound(final GameDto game, final RoundDto roundEntity) throws GameWsApiException;

    public GameDto restart(PlayerDto player) throws GameWsApiException;

    public GameTotalInfoDto findGameTotalInfo();

}

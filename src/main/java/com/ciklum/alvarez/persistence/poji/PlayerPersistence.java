package com.ciklum.alvarez.persistence.poji;

import java.util.List;
import java.util.Optional;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;

public interface PlayerPersistence {

    public void createPlayer(PlayerDto player) throws GameWsApiException;

    public Optional<PlayerDto> findPlayerById(String playerId) throws GameWsApiException;

    public List<PlayerDto> findAllPlayers() throws GameWsApiException;

}

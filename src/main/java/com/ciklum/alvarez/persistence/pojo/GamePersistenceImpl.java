package com.ciklum.alvarez.persistence.pojo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.ciklum.alvarez.common.pojo.GameSingletonBean;
import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.model.entity.GameEntity;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.ciklum.alvarez.persistence.poji.GamePersistence;

/**
 *
 * @author agustin.alvarez
 *
 */
@Component
public class GamePersistenceImpl implements GamePersistence {

    private static final String MSG_ERROR_GAME_NOT_EXIST = "Game doesn't exist.";

    @Autowired
    private GameSingletonBean gameSingletonBean;

    @Autowired
    private PersistenceMapper gamePersistenceMapper;

    @Override
    public GameDto createGame(final PlayerDto playerDto) throws GameWsApiException {
	final GameEntity gameEntity = this.buildGameEntity(playerDto);

	this.gameSingletonBean.insert(gameEntity);

	return this.gamePersistenceMapper.mapGameEntityToGameDto(gameEntity);
    }

    @Override
    public Optional<GameDto> findGameByUUID(final UUID gameId) throws GameWsApiException {
	return this.gameSingletonBean.get(gameId)//
		.map(this.gamePersistenceMapper::mapGameEntityToGameDto);
    }

    @Override
    public Optional<GameDto> findGameByPlayerEmail(final String email) throws GameWsApiException {
	return this.gameSingletonBean.getValues()//
		.stream()//
		.filter(Objects::nonNull)//
		.filter(game -> email.equals(game.getPlayerOne().getUserName()))//
		.findFirst()//
		.map(this.gamePersistenceMapper::mapGameEntityToGameDto);

    }

    @Override
    public List<GameDto> findAllGames() throws GameWsApiException {
	return this.gamePersistenceMapper.mapGameEntityListToGameDtoList(this.gameSingletonBean.getValues());
    }

    @Override
    public void insertRound(final GameDto game, final RoundDto roundDto) throws GameWsApiException {
	this.gameSingletonBean.get(game.getGameId())//
		.ifPresent(gameEntity -> this.processInsertRound(game, roundDto, gameEntity));

    }

    @Override
    public GameDto restart(final PlayerDto playerEntity) throws GameWsApiException {
	final GameDto gameDto = this.findGameByPlayerEmail(playerEntity.getUserName())//
		.orElseThrow(() -> GameWsApiException.builder().defaultMessage(MSG_ERROR_GAME_NOT_EXIST).statusCode(HttpStatus.NOT_FOUND).build());

	return this.gameSingletonBean.get(gameDto.getGameId())//
		.map(this::restartValuesAndMapToGameDto)//
		.orElseThrow(() -> GameWsApiException.builder().defaultMessage(MSG_ERROR_GAME_NOT_EXIST).statusCode(HttpStatus.NOT_FOUND).build());

    }

    @Override
    public GameTotalInfoDto findGameTotalInfo() {
	return GameTotalInfoDto.builder()//
		.totalDraws(this.gameSingletonBean.getTotalDraws())//
		.totalPlayerOneWins(this.gameSingletonBean.getTotalPlayerOneWins())//
		.totalPlayerTwoWins(this.gameSingletonBean.getTotalPlayerTwoWins())//
		.totalRounds(this.gameSingletonBean.getTotalRounds())//
		.build();
    }

    // ~ Private methods ==================================================

    private void processInsertRound(final GameDto game, final RoundDto roundDto, final GameEntity gameEntity) {
	gameEntity.getRounds().add(this.gamePersistenceMapper.mapRoundDtoToRoundEntity(roundDto));
	gameEntity.setCount(game.getCount() + 1);

	switch (roundDto.getResult()) {
	case PLAYER1_WINS:
	    this.gameSingletonBean.incrementTotalPlayerOneWins();
	    break;
	case PLAYER2_WINS:
	    this.gameSingletonBean.incrementTotalPlayerTwoWins();
	    break;
	default:
	    this.gameSingletonBean.incrementTotalDraws();
	    break;
	}
    }

    private GameDto restartValuesAndMapToGameDto(final GameEntity gameEntity) {
	gameEntity.setCount(0L);
	gameEntity.setRounds(new ArrayList<>());
	return this.gamePersistenceMapper.mapGameEntityToGameDto(gameEntity);
    }

    private PlayerEntity buildUserTwo() {
	return PlayerEntity.builder()//
		.userName("elon.musk@tesla.com")//
		.build();
    }

    private GameEntity buildGameEntity(final PlayerDto user) {
	return GameEntity.builder()//
		.gameId(UUID.randomUUID())//
		.playerOne(this.gamePersistenceMapper.mapPlayerDtoToPlayerEntity(user))//
		.playerTwo(this.buildUserTwo())//
		.rounds(new ArrayList<>())//
		.createdAt(LocalDateTime.now())//
		.build();
    }

}

package com.ciklum.alvarez.persistence.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.model.entity.GameEntity;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.ciklum.alvarez.model.entity.RoundEntity;

@Component
public class PersistenceMapper {

    public GameDto mapGameEntityToGameDto(final GameEntity gameEntity) {

	if (gameEntity == null) {
	    return null;
	}

	return GameDto.builder()//
		.gameId(gameEntity.getGameId())//
		.count(gameEntity.getCount())//
		.playerOne(this.mapPlayerEntityToPlayerDto(gameEntity.getPlayerOne()))//
		.playerTwo(this.mapPlayerEntityToPlayerDto(gameEntity.getPlayerTwo()))//
		.rounds(this.mapRoundEntityListToRoundDtoList(gameEntity.getRounds()))//
		.build();
    }

    public List<GameDto> mapGameEntityListToGameDtoList(final List<GameEntity> gameEntity) {
	if (CollectionUtils.isEmpty(gameEntity)) {
	    return new ArrayList<>();
	}

	return gameEntity.stream()//
		.filter(Objects::nonNull)//
		.map(this::mapGameEntityToGameDto)//
		.collect(Collectors.toList());
    }

    public PlayerDto mapPlayerEntityToPlayerDto(final PlayerEntity playerEntity) {
	if (playerEntity == null) {
	    return null;
	}

	return PlayerDto.builder()//
		.userName(playerEntity.getUserName())//
		.password(playerEntity.getPassword())//
		.build();
    }

    public RoundDto mapRoundEntityToRoundDto(final RoundEntity roundEntity) {

	if (roundEntity == null) {
	    return null;
	}

	return RoundDto.builder()//
		.roundId(roundEntity.getRoundId())//
		.playerOneChoice(roundEntity.getPlayerOneChoice())//
		.playerTwoChoice(roundEntity.getPlayerTwoChoice())//
		.result(roundEntity.getResult())//
		.build();
    }

    public List<RoundDto> mapRoundEntityListToRoundDtoList(final List<RoundEntity> roundEntity) {

	if (CollectionUtils.isEmpty(roundEntity)) {
	    return new ArrayList<>();
	}

	return roundEntity.stream()//
		.filter(Objects::nonNull)//
		.map(this::mapRoundEntityToRoundDto)//
		.collect(Collectors.toList());

    }

    public PlayerEntity mapPlayerDtoToPlayerEntity(final PlayerDto playerDto) {
	if (playerDto == null) {
	    return null;
	}

	return PlayerEntity.builder()//
		.userName(playerDto.getUserName())//
		.password(playerDto.getPassword())//
		.build();
    }

    public RoundEntity mapRoundDtoToRoundEntity(final RoundDto roundDto) {
	return RoundEntity.builder()//
		.roundId(roundDto.getRoundId())//
		.playerOneChoice(roundDto.getPlayerOneChoice())//
		.playerTwoChoice(roundDto.getPlayerTwoChoice())//
		.result(roundDto.getResult())//
		.build();
    }

}

package com.ciklum.alvarez.persistence.pojo;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.common.pojo.PlayerSingletonBean;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.ciklum.alvarez.persistence.poji.PlayerPersistence;

@Component
public class PlayerPersistenceImpl implements PlayerPersistence {

    @Autowired
    private PlayerSingletonBean playerSingleton;

    @Autowired
    private PersistenceMapper persistenceMapper;

    @Override
    public void createPlayer(final PlayerDto playerDto) throws GameWsApiException {
	final PlayerEntity playerEntity = PlayerEntity.builder()//
		.userName(playerDto.getUserName())//
		.password(playerDto.getPassword())//
		.build();

	this.playerSingleton.insert(playerEntity);
    }

    @Override
    public Optional<PlayerDto> findPlayerById(final String key) throws GameWsApiException {
	return this.playerSingleton.get(key)//
		.map(this.persistenceMapper::mapPlayerEntityToPlayerDto);
    }

    @Override
    public List<PlayerDto> findAllPlayers() throws GameWsApiException {
	return this.playerSingleton.getValues().stream()//
		.filter(Objects::nonNull)//
		.map(this.persistenceMapper::mapPlayerEntityToPlayerDto)//
		.collect(Collectors.toList());
    }

}

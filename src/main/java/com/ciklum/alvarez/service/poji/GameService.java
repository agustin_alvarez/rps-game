package com.ciklum.alvarez.service.poji;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.service.pojo.dto.GameResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.GameTotalInfoResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayerRequestJsonDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "GameServiceRest")
@RequestMapping(value = "/game")
public interface GameService {

    @ApiOperation(value = "Get the total information about all the games")
    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameTotalInfoResponseJsonDto> getGameTotalInfo();

    @ApiOperation(value = "Get a list of the all games")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GameResponseJsonDto>> getGames() throws GameWsApiException;

    @ApiOperation(value = "Get the detail of the object")
    @GetMapping(value = "/{gameId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameResponseJsonDto> getGame(@ApiParam(value = "GameId", required = true) @PathVariable("gameId") final String gameId) throws GameWsApiException;

    @ApiOperation(value = "Create a game for the current player")
    @PostMapping
    public ResponseEntity<String> createGame(@Validated @RequestBody PlayerRequestJsonDto playerId) throws GameWsApiException;

    @ApiOperation(value = "Play a new Round of the game")
    @PostMapping(value = "/play", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameResponseJsonDto> play(@Validated @RequestBody PlayRequestJsonDto playRequestDto) throws GameWsApiException;

    @ApiOperation(value = "Restart the game for the current player")
    @PutMapping(value = "/restart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> restart(@Validated @RequestBody PlayRequestJsonDto playRequestDto) throws GameWsApiException;

}

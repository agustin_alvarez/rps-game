package com.ciklum.alvarez.service.poji;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.service.pojo.dto.PlayerRequestJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayerResponseJsonDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "PlayerServiceRest")
@RequestMapping(value = "/player")
public interface PlayerService {

    @ApiOperation(value = "Create a new player")
    @PostMapping(value = "/signup")
    public ResponseEntity<String> createPlayer(@RequestBody PlayerRequestJsonDto playerRequestDto) throws GameWsApiException;

    @ApiOperation(value = "Get a list of players")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PlayerResponseJsonDto>> getPlayers() throws GameWsApiException;

}

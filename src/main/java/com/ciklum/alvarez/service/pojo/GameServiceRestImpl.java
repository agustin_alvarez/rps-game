package com.ciklum.alvarez.service.pojo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.GameDomainImpl;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.service.poji.GameService;
import com.ciklum.alvarez.service.pojo.dto.GameResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.GameTotalInfoResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayerRequestJsonDto;
import com.ciklum.alvarez.service.pojo.dto.RoundResponseJsonDto;

@RestController
public class GameServiceRestImpl implements GameService {

    @Autowired
    private GameDomainImpl gameDomain;

    @Override
    public ResponseEntity<GameTotalInfoResponseJsonDto> getGameTotalInfo() {
	final GameTotalInfoDto gameTotalInfoDto = this.gameDomain.findGameTotalInfo();

	final GameTotalInfoResponseJsonDto response = GameTotalInfoResponseJsonDto.builder()//
		.totalDraws(gameTotalInfoDto.getTotalDraws())//
		.totalPlayerOneWins(gameTotalInfoDto.getTotalPlayerOneWins())//
		.totalPlayerTwoWins(gameTotalInfoDto.getTotalPlayerTwoWins())//
		.totalRounds(gameTotalInfoDto.getTotalRounds())//
		.build();

	return ResponseEntity.ok()//
		.body(response);
    }

    @Override
    public ResponseEntity<GameResponseJsonDto> getGame(final String gameId) throws GameWsApiException {
	final Optional<GameDto> gameDto = this.gameDomain.findGameById(gameId);

	if (!gameDto.isPresent()) {
	    throw GameWsApiException.builder().defaultMessage("Couldn't find the game").statusCode(HttpStatus.NOT_FOUND).build();
	}

	final GameResponseJsonDto response = this.mapGameDtoToGameResponseDto(gameDto.get());
	return ResponseEntity.ok()//
		.body(response);
    }

    @Override
    public ResponseEntity<List<GameResponseJsonDto>> getGames() throws GameWsApiException {

	final List<GameResponseJsonDto> response = this.gameDomain.findAllGames().stream()//
		.map(this::mapGameDtoToGameResponseDto)//
		.collect(Collectors.toList());

	return ResponseEntity.ok()//
		.body(response);
    }

    @Override
    public ResponseEntity<String> createGame(final PlayerRequestJsonDto playerRequestJsonDto) throws GameWsApiException {
	final GameDto game = this.gameDomain.createGame(playerRequestJsonDto.getUserName());

	return ResponseEntity.status(HttpStatus.CREATED)//
		.body(game.getGameId().toString());

    }

    @Override
    public ResponseEntity<GameResponseJsonDto> play(final PlayRequestJsonDto playRequestDto) throws GameWsApiException {
	final GameDto gameDto = this.gameDomain.play(playRequestDto);

	final GameResponseJsonDto response = this.mapGameDtoToGameResponseDto(gameDto);
	return ResponseEntity.ok()//
		.body(response);
    }

    @Override
    public ResponseEntity<String> restart(final PlayRequestJsonDto playRequestDto) throws GameWsApiException {

	this.gameDomain.restart(playRequestDto.getUserName());

	return ResponseEntity.ok()//
		.body("Operation succesfully");
    }

    // ~ Private methods ============================================

    private RoundResponseJsonDto mapRoundToToRoundResponseDto(final RoundDto roundEntity) {
	return RoundResponseJsonDto.builder()//
		.playerOneChoice(roundEntity.getPlayerOneChoice())//
		.playerTwoChoice(roundEntity.getPlayerTwoChoice())//
		.result(roundEntity.getResult())//
		.build();
    }

    private GameResponseJsonDto mapGameDtoToGameResponseDto(final GameDto gameDto) {
	return GameResponseJsonDto.builder()//
		.gameId(gameDto.getGameId().toString()) //
		.playerOne(gameDto.getPlayerOne().getUserName())//
		.playerTwo(gameDto.getPlayerTwo().getUserName())//
		.count(gameDto.getCount())//
		.rounds(gameDto.getRounds().stream()//
			.map(this::mapRoundToToRoundResponseDto)//
			.collect(Collectors.toList()))
		.build();
    }
}

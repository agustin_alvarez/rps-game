package com.ciklum.alvarez.service.pojo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.poji.PlayerDomain;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.service.poji.PlayerService;
import com.ciklum.alvarez.service.pojo.dto.PlayerRequestJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayerResponseJsonDto;

@RestController
public class PlayerServiceRestImpl implements PlayerService {

    @Autowired
    private PlayerDomain playerDomain;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public ResponseEntity<String> createPlayer(final PlayerRequestJsonDto playerRequestDto) throws GameWsApiException {
	final PlayerDto player = PlayerDto.builder()//
		.userName(playerRequestDto.getUserName())//
		.password(this.bCryptPasswordEncoder.encode(playerRequestDto.getPassword()))//
		.build();

	this.playerDomain.createPlayer(player);

	return ResponseEntity.status(HttpStatus.CREATED)//
		.body(player.getUserName());
    }

    @Override
    public ResponseEntity<List<PlayerResponseJsonDto>> getPlayers() throws GameWsApiException {
	final List<PlayerResponseJsonDto> response = this.playerDomain.findAllPlayers().stream()//
		.map(gameEntity -> PlayerResponseJsonDto.builder()//
			.userName(gameEntity.getUserName())//
			.build())
		.collect(Collectors.toList());

	return ResponseEntity.status(HttpStatus.OK)//
		.body(response);
    }

}

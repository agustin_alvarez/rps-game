package com.ciklum.alvarez.service.pojo.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@EqualsAndHashCode
@ToString
@ApiModel(value = "GameResponseJsonDto", description = "Response with game information")
public class GameResponseJsonDto {

    @ApiModelProperty(value = "ID GameId", allowEmptyValue = false, required = true, example = "00321e5e-1017-4725-baa7-87c09e4fb1e7")
    private final String gameId;
    
    @ApiModelProperty(value = "Username of player one", allowEmptyValue = false, required = true, example = "agustin@test.com")
    private String playerOne;
    
    @ApiModelProperty(value = "Username of player two", allowEmptyValue = false, required = true, example = "agustin2@test.com")
    private String playerTwo;
    
    @Builder.Default
    @ApiModelProperty(value = "Number of rounds per game", allowEmptyValue = false, required = true, example = "25")
    private Long count = 0L;
    
    @Builder.Default
    private List<RoundResponseJsonDto> rounds = new ArrayList<>();
}

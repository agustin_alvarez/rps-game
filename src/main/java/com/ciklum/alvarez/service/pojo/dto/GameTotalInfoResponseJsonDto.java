package com.ciklum.alvarez.service.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
@ApiModel(value = "GameTotalInfoResponseJsonDto", description = "Response with the data of all the games")
public class GameTotalInfoResponseJsonDto {

    @ApiModelProperty(value = "Total Player One Wins", allowEmptyValue = false, required = true, example = "12")
    private final Long totalPlayerOneWins;

    @ApiModelProperty(value = "Total Player Two Wins", allowEmptyValue = false, required = true, example = "11")
    private final Long totalPlayerTwoWins;

    @ApiModelProperty(value = "Total Draws", allowEmptyValue = false, required = true, example = "10")
    private final Long totalDraws;

    @ApiModelProperty(value = "Total Rounds", allowEmptyValue = false, required = true, example = "33")
    private final Long totalRounds;
}

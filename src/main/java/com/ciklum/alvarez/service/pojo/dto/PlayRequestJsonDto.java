package com.ciklum.alvarez.service.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "PlayRequestJsonDto")
public class PlayRequestJsonDto {

    @ApiModelProperty(value = "User Name", allowEmptyValue = false, required = true, example = "agustin@test.com")
    private String userName;

    @ApiModelProperty(value = "Game Id", allowEmptyValue = false, required = true, example = "5c66fab2-7eab-4ab2-a8eb-31a749496c27")
    private String gameId;

}

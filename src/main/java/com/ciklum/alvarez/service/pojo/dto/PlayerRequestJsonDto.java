package com.ciklum.alvarez.service.pojo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "PlayerRequestJsonDto", description = "Request to update player Info")
public class PlayerRequestJsonDto {

    @NotNull
    @NotBlank
    @ApiModelProperty(value = "User name", allowEmptyValue = false, required = true, example = "agustin@test.com")
    private String userName;

    @ApiModelProperty(value = "Password", allowEmptyValue = true, required = false, example = "IamPassword1234")
    private String password;

}
package com.ciklum.alvarez.service.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "PlayerResponseJsonDto")
public class PlayerResponseJsonDto {

    @ApiModelProperty(value = "User Name", allowEmptyValue = false, required = true, example = "agustin@test.com")
    private String userName;

}

package com.ciklum.alvarez.service.pojo.dto;

import com.ciklum.alvarez.common.enums.RoundResultEnum;
import com.ciklum.alvarez.common.enums.TypeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
@ApiModel(value = "RoundResponseJsonDto")
public class RoundResponseJsonDto {

    @ApiModelProperty(value = "User Name", allowEmptyValue = false, required = true, allowableValues = "ROCK, PAPER, SCISSOR")
    private final TypeEnum playerOneChoice;

    @ApiModelProperty(value = "User Name", allowEmptyValue = false, required = true, allowableValues = "ROCK, PAPER, SCISSOR")
    private final TypeEnum playerTwoChoice;

    @ApiModelProperty(value = "User Name", allowEmptyValue = false, required = true, allowableValues = "PLAYER1_WINS, PLAYER2_WINS, DRAW")
    private final RoundResultEnum result;
}

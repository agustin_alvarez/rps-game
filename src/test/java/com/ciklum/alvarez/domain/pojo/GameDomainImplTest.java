package com.ciklum.alvarez.domain.pojo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.poji.PlayerDomain;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.persistence.poji.GamePersistence;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class GameDomainImplTest {

    @InjectMocks
    private GameDomainImpl gameDomainImpl;

    @Mock
    private GamePersistence gamePersistence;

    @Mock
    private PlayerDomain playerDomain;

    private static final String EXPECTED_PLAYER_ID = "0000-1111";
    private static final String EXPECTED_USERNAME = "test@test.com";

    @Test
    void testFindGameTotalInfo() {
	final GameTotalInfoDto expectedGameTotalInfoDto = GameTotalInfoDto.builder()//
		.build();

	BDDMockito.given(this.gamePersistence.findGameTotalInfo()).willReturn(expectedGameTotalInfoDto);

	final GameTotalInfoDto actualGameTotalInfoDto = this.gameDomainImpl.findGameTotalInfo();

	Assertions.assertThat(actualGameTotalInfoDto) //
		.isNotNull();
    }

    @Test
    void testFindGameById() throws GameWsApiException {
	final Optional<GameDto> expectedGameDto = Optional.of(//
		GameDto.builder()//
			.build());

	BDDMockito.given(this.gamePersistence.findGameByUUID(BDDMockito.any())).willReturn(expectedGameDto);

	final Optional<GameDto> actualGameDto = this.gameDomainImpl.findGameById(UUID.randomUUID().toString());

	Assertions.assertThat(actualGameDto) //
		.isNotNull();
    }

    @Test
    void testFindAllGames() throws GameWsApiException {
	BDDMockito.given(this.gamePersistence.findAllGames()).willReturn(ImmutableList.of());

	final List<GameDto> actualGameDto = this.gameDomainImpl.findAllGames();

	Assertions.assertThat(actualGameDto) //
		.isNotNull();
    }

    @Test
    void testCreateGame() throws GameWsApiException {
	final Optional<PlayerDto> expectedPlayerDto = Optional.of(//
		PlayerDto.builder()//
			.userName(EXPECTED_USERNAME)//
			.build());
	final GameDto expectedGameDto = GameDto.builder().build();

	BDDMockito.given(this.playerDomain.findPlayerById(BDDMockito.any())).willReturn(expectedPlayerDto);
	BDDMockito.given(this.gamePersistence.createGame(BDDMockito.any())).willReturn(expectedGameDto);

	final GameDto actualGameDto = this.gameDomainImpl.createGame(EXPECTED_PLAYER_ID);

	Assertions.assertThat(actualGameDto) //
		.isNotNull();
    }

    @Test
    void testCreateGameWithException() throws GameWsApiException {
	final Optional<PlayerDto> expectedPlayerDto = Optional.empty();

	BDDMockito.given(this.playerDomain.findPlayerById(BDDMockito.any())).willReturn(expectedPlayerDto);

	final GameWsApiException assertThrows = org.junit.jupiter.api.Assertions.assertThrows(//
		GameWsApiException.class, () -> {
		    this.gameDomainImpl.createGame(EXPECTED_PLAYER_ID);
		});

	org.junit.jupiter.api.Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, assertThrows.getStatusCode());
    }

    @Test
    void testRestart() throws GameWsApiException {
	final Optional<PlayerDto> expectedPlayerDto = Optional.of(//
		PlayerDto.builder()//
			.userName(EXPECTED_USERNAME)//
			.build());

	final GameDto expectedGameDto = GameDto.builder().build();
	BDDMockito.given(this.playerDomain.findPlayerById(BDDMockito.any())).willReturn(expectedPlayerDto);
	BDDMockito.given(this.gamePersistence.restart(BDDMockito.any())).willReturn(expectedGameDto);

	final GameDto actualGameDto = this.gameDomainImpl.restart(EXPECTED_PLAYER_ID);

	Assertions.assertThat(actualGameDto) //
		.isNotNull();
    }

    @Test
    void testRestartWithException() throws GameWsApiException {
	final Optional<PlayerDto> expectedPlayerDto = Optional.empty();

	BDDMockito.given(this.playerDomain.findPlayerById(BDDMockito.any())).willReturn(expectedPlayerDto);

	final GameWsApiException assertThrows = org.junit.jupiter.api.Assertions.assertThrows(//
		GameWsApiException.class, () -> {
		    this.gameDomainImpl.restart(EXPECTED_PLAYER_ID);
		});

	org.junit.jupiter.api.Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, assertThrows.getStatusCode());
    }

    @Test
    void testPlay() throws GameWsApiException {
	final PlayRequestJsonDto expectedPlayRequestDto = PlayRequestJsonDto.builder().build();
	final Optional<GameDto> expectedGameDto = Optional.of(GameDto.builder().gameId(UUID.randomUUID()).build());

	BDDMockito.given(this.gamePersistence.findGameByPlayerEmail(BDDMockito.any())).willReturn(expectedGameDto);
	BDDMockito.doNothing().when(this.gamePersistence).insertRound(BDDMockito.any(), BDDMockito.any());
	BDDMockito.given(this.gamePersistence.findGameByUUID(BDDMockito.any())).willReturn(expectedGameDto);

	final GameDto actualGameDto = this.gameDomainImpl.play(expectedPlayRequestDto);

	Assertions.assertThat(actualGameDto) //
		.isNotNull();
    }

}

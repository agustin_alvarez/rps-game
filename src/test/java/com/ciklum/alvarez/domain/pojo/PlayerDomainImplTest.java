package com.ciklum.alvarez.domain.pojo;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.persistence.poji.PlayerPersistence;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class PlayerDomainImplTest {

    @InjectMocks
    private PlayerDomainImpl playerDomainImpl;

    @Mock
    private PlayerPersistence playerPersistence;

    @Test
    void testFindPlayerById() throws GameWsApiException {
	final Optional<PlayerDto> expectedPlayerDto = Optional.of(PlayerDto.builder().build());

	BDDMockito.given(this.playerPersistence.findPlayerById(BDDMockito.any())).willReturn(expectedPlayerDto);

	final Optional<PlayerDto> actualPlayerDto = this.playerDomainImpl.findPlayerById(BDDMockito.any());

	Assertions.assertThat(actualPlayerDto) //
		.isNotNull();
    }

    @Test
    void testFindAllPlayers() throws GameWsApiException {
	final List<PlayerDto> expectedPlayerDto = ImmutableList.of(PlayerDto.builder().build());

	BDDMockito.given(this.playerPersistence.findAllPlayers()).willReturn(expectedPlayerDto);

	final List<PlayerDto> actualPlayerDto = this.playerDomainImpl.findAllPlayers();

	Assertions.assertThat(actualPlayerDto) //
		.isNotNull();
    }

    @Test
    void testCreatePlayer() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().userName("my-email@test.com").build();

	this.playerDomainImpl.createPlayer(expectedPlayerDto);

	Mockito.verify(this.playerPersistence, Mockito.times(1)).createPlayer(BDDMockito.any());
    }

    @Test
    void testCreatePlayerWithException() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = null;

	final GameWsApiException assertThrows = org.junit.jupiter.api.Assertions.assertThrows(//
		GameWsApiException.class, () -> {
		    this.playerDomainImpl.createPlayer(expectedPlayerDto);
		});

	org.junit.jupiter.api.Assertions.assertEquals(HttpStatus.BAD_REQUEST, assertThrows.getStatusCode());

    }

    @Test
    void testCreatePlayerWithExceptionBecauseEmail() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().build();

	final GameWsApiException assertThrows = org.junit.jupiter.api.Assertions.assertThrows(//
		GameWsApiException.class, () -> {
		    this.playerDomainImpl.createPlayer(expectedPlayerDto);
		});

	org.junit.jupiter.api.Assertions.assertEquals(HttpStatus.BAD_REQUEST, assertThrows.getStatusCode());

    }

}

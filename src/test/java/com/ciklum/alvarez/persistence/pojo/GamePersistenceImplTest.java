package com.ciklum.alvarez.persistence.pojo;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.CollectionUtils;

import com.ciklum.alvarez.common.enums.TypeEnum;
import com.ciklum.alvarez.common.pojo.GameSingletonBean;
import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.model.entity.GameEntity;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class GamePersistenceImplTest {

    @InjectMocks
    private GamePersistenceImpl gamePersistenceImpl;

    @Mock
    private GameSingletonBean gameSingletonBean;

    @Mock
    private PersistenceMapper persistenceMapper;

    private static final String EXPECTED_USERNAME = "email@test.com";

    @Test
    void testCreateGame() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().userName(EXPECTED_USERNAME).build();
	final PlayerEntity expectedPlayerEntity = PlayerEntity.builder().userName(EXPECTED_USERNAME).build();

	BDDMockito.given(this.persistenceMapper.mapPlayerDtoToPlayerEntity(BDDMockito.any())).willReturn(expectedPlayerEntity);
	BDDMockito.doNothing().when(this.gameSingletonBean).insert(BDDMockito.any());

	this.gamePersistenceImpl.createGame(expectedPlayerDto);

	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapGameEntityToGameDto(BDDMockito.any());
    }

    @Test
    void testFindGameByUUID() throws GameWsApiException {
	final Optional<GameEntity> expectedGameEntity = Optional.of(GameEntity.builder().build());
	final GameDto expectedGameDto = GameDto.builder().build();

	BDDMockito.given(this.gameSingletonBean.get(BDDMockito.any())).willReturn(expectedGameEntity);
	BDDMockito.given(this.persistenceMapper.mapGameEntityToGameDto(BDDMockito.any())).willReturn(expectedGameDto);

	this.gamePersistenceImpl.findGameByUUID(BDDMockito.any());

	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapGameEntityToGameDto(expectedGameEntity.get());
    }

    @Test
    void testFindGameByPlayerEmail() throws GameWsApiException {
	final GameEntity expectedGameEntity = GameEntity.builder().playerOne(PlayerEntity.builder().userName(EXPECTED_USERNAME).build()).count(0L).build();
	final GameDto expectedGameDto = GameDto.builder().playerOne(PlayerDto.builder().userName(EXPECTED_USERNAME).build()).count(0L).build();

	BDDMockito.given(this.gameSingletonBean.getValues()).willReturn(ImmutableList.of(expectedGameEntity));
	BDDMockito.given(this.persistenceMapper.mapGameEntityToGameDto(BDDMockito.any())).willReturn(expectedGameDto);

	this.gamePersistenceImpl.findGameByPlayerEmail(EXPECTED_USERNAME);

	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapGameEntityToGameDto(expectedGameEntity);
    }

    @Test
    void testRestart() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().userName(EXPECTED_USERNAME).build();
	final Optional<GameEntity> expectedGameEntity = Optional.of(//
		GameEntity.builder().playerOne(//
			PlayerEntity.builder().userName(EXPECTED_USERNAME).build()).count(0L).build());
	final GameDto expectedGameDto = GameDto.builder().count(0L).build();

	BDDMockito.given(this.gameSingletonBean.getValues()).willReturn(ImmutableList.of(expectedGameEntity.get()));
	BDDMockito.given(this.persistenceMapper.mapGameEntityToGameDto(BDDMockito.any())).willReturn(expectedGameDto);
	BDDMockito.given(this.gameSingletonBean.get(BDDMockito.any())).willReturn(expectedGameEntity);

	final GameDto actualGameDto = this.gamePersistenceImpl.restart(expectedPlayerDto);

	Assertions.assertThat(actualGameDto) //
		.isNotNull()//
		.matches(actual -> actual.getCount().equals(0L))//
		.matches(actual -> CollectionUtils.isEmpty(actual.getRounds()))//
	;
    }

    @Test
    void testFindAllGames() throws GameWsApiException {
	this.gamePersistenceImpl.findAllGames();

	Mockito.verify(this.gameSingletonBean, Mockito.times(1)).getValues();
	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapGameEntityListToGameDtoList(BDDMockito.any());
    }

    @Test
    void testInsertRoundPlayerOneWins() throws GameWsApiException {
	final GameDto expectedGameDto = GameDto.builder().count(0L).build();
	final RoundDto expectedRoundDto = this.buildRoundDto(TypeEnum.PAPER, TypeEnum.ROCK);
	final Optional<GameEntity> expectedGameEntity = Optional.of(GameEntity.builder().count(0L).build());

	BDDMockito.given(this.gameSingletonBean.get(BDDMockito.any())).willReturn(expectedGameEntity);

	this.gamePersistenceImpl.insertRound(expectedGameDto, expectedRoundDto);

	Mockito.verify(this.gameSingletonBean, Mockito.times(1)).incrementTotalPlayerOneWins();
    }

    @Test
    void testInsertRoundPlayerTwoWins() throws GameWsApiException {
	final GameDto expectedGameDto = GameDto.builder().count(0L).build();
	final RoundDto expectedRoundDto = this.buildRoundDto(TypeEnum.SCISSOR, TypeEnum.ROCK);
	final Optional<GameEntity> expectedGameEntity = Optional.of(GameEntity.builder().count(0L).build());

	BDDMockito.given(this.gameSingletonBean.get(BDDMockito.any())).willReturn(expectedGameEntity);

	this.gamePersistenceImpl.insertRound(expectedGameDto, expectedRoundDto);

	Mockito.verify(this.gameSingletonBean, Mockito.times(1)).incrementTotalPlayerTwoWins();
    }

    @Test
    void testInsertRoundDraw() throws GameWsApiException {
	final GameDto expectedGameDto = GameDto.builder().count(0L).build();
	final RoundDto expectedRoundDto = this.buildRoundDto(TypeEnum.ROCK, TypeEnum.ROCK);
	final Optional<GameEntity> expectedGameEntity = Optional.of(GameEntity.builder().count(0L).build());

	BDDMockito.given(this.gameSingletonBean.get(BDDMockito.any())).willReturn(expectedGameEntity);

	this.gamePersistenceImpl.insertRound(expectedGameDto, expectedRoundDto);

	Mockito.verify(this.gameSingletonBean, Mockito.times(1)).incrementTotalDraws();
    }

    @Test
    void testFindGameTotalInfo() {
	final Long expectedTotalDraws = 3L;
	final Long expectedTotalPlayerOneWins = 4L;
	final Long expectedTotalPlayerTwoWins = 5L;
	final Long expectedTotalRounds = 12L;

	BDDMockito.given(this.gameSingletonBean.getTotalDraws()).willReturn(expectedTotalDraws);
	BDDMockito.given(this.gameSingletonBean.getTotalPlayerOneWins()).willReturn(expectedTotalPlayerOneWins);
	BDDMockito.given(this.gameSingletonBean.getTotalPlayerTwoWins()).willReturn(expectedTotalPlayerTwoWins);
	BDDMockito.given(this.gameSingletonBean.getTotalRounds()).willReturn(expectedTotalRounds);

	final GameTotalInfoDto actualTotalInfo = this.gamePersistenceImpl.findGameTotalInfo();

	Assertions.assertThat(actualTotalInfo) //
		.isNotNull()//
		.matches(actual -> actual.getTotalDraws().equals(expectedTotalDraws))//
		.matches(actual -> actual.getTotalPlayerOneWins().equals(expectedTotalPlayerOneWins))//
		.matches(actual -> actual.getTotalPlayerTwoWins().equals(expectedTotalPlayerTwoWins))//
		.matches(actual -> actual.getTotalRounds().equals(expectedTotalRounds))//
	;
    }

    private RoundDto buildRoundDto(final TypeEnum playerOne, final TypeEnum playerTwo) {
	return RoundDto.builder()//
		.playerOneChoice(playerOne)//
		.playerTwoChoice(playerTwo)//
		.result(TypeEnum.wins(playerOne, playerTwo))//
		.build();
    }
}

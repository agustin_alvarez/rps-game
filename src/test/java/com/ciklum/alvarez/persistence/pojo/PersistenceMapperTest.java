package com.ciklum.alvarez.persistence.pojo;

import java.util.List;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.CollectionUtils;

import com.ciklum.alvarez.common.enums.RoundResultEnum;
import com.ciklum.alvarez.common.enums.TypeEnum;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.domain.pojo.dto.RoundDto;
import com.ciklum.alvarez.model.entity.GameEntity;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.ciklum.alvarez.model.entity.RoundEntity;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class PersistenceMapperTest {

    @InjectMocks
    private PersistenceMapper persistenceMapper;

    private static final String EXPECTED_USERNAME = "email@test.com";

    @Test
    void testMapGameEntityToGameDto() {
	final UUID expectedGameId = UUID.randomUUID();
	final Long expectedCount = 12L;
	final PlayerEntity expectedPlayerOneEntity = PlayerEntity.builder().userName(EXPECTED_USERNAME).build();
	final PlayerEntity expectedPlayerTwoEntity = PlayerEntity.builder().userName("email2@test.com").build();
	final List<RoundEntity> expectedRounds = ImmutableList.of();

	final GameEntity expectedGameEntity = GameEntity.builder()//
		.gameId(expectedGameId)//
		.count(expectedCount)//
		.playerOne(expectedPlayerOneEntity)//
		.playerTwo(expectedPlayerTwoEntity)//
		.rounds(expectedRounds)//
		.build();

	final GameDto actualGameDto = this.persistenceMapper.mapGameEntityToGameDto(expectedGameEntity);

	Assertions.assertThat(actualGameDto) //
		.isNotNull()//
		.matches(actual -> actual.getGameId().equals(expectedGameId))//
		.matches(actual -> actual.getCount().equals(expectedCount))//
		.matches(actual -> actual.getPlayerOne().getUserName().equals(expectedPlayerOneEntity.getUserName()))//
		.matches(actual -> actual.getPlayerTwo().getUserName().equals(expectedPlayerTwoEntity.getUserName()))//
		.matches(actual -> actual.getPlayerTwo().getUserName().equals(expectedPlayerTwoEntity.getUserName()))//
		.matches(actual -> CollectionUtils.isEmpty(actual.getRounds()))//
	;
    }

    @Test
    void testMapGameEntityToGameDtoNull() {
	final GameEntity expectedGameEntity = null;

	final GameDto actualGameDto = this.persistenceMapper.mapGameEntityToGameDto(expectedGameEntity);

	Assertions.assertThat(actualGameDto).isNull();
    }

    @Test
    void testMapPlayerEntityToPlayerDto() {
	final PlayerEntity expectedPlayerEntity = PlayerEntity.builder().userName(EXPECTED_USERNAME).build();

	final PlayerDto actualPlayerDto = this.persistenceMapper.mapPlayerEntityToPlayerDto(expectedPlayerEntity);

	Assertions.assertThat(actualPlayerDto) //
		.isNotNull()//
		.matches(actual -> actual.getUserName().equals(EXPECTED_USERNAME));
    }

    @Test
    void testMapPlayerEntityToPlayerDtoNull() {
	final PlayerEntity expectedPlayerEntity = null;

	final PlayerDto actualPlayerDto = this.persistenceMapper.mapPlayerEntityToPlayerDto(expectedPlayerEntity);

	Assertions.assertThat(actualPlayerDto).isNull();
    }

    @Test
    void testMapRoundEntityToRoundDto() {
	final UUID expectedRoundId = UUID.randomUUID();
	final TypeEnum expectedPlayerOneChoice = TypeEnum.PAPER;
	final TypeEnum expectedPlayerTwoChoice = TypeEnum.ROCK;
	final RoundResultEnum expectedRoundResult = RoundResultEnum.PLAYER1_WINS;

	final RoundEntity expectedPlayerEntity = RoundEntity.builder()//
		.roundId(expectedRoundId)//
		.playerOneChoice(expectedPlayerOneChoice)//
		.playerTwoChoice(expectedPlayerTwoChoice)//
		.result(expectedRoundResult)//
		.build();

	final RoundDto actualRoundDto = this.persistenceMapper.mapRoundEntityToRoundDto(expectedPlayerEntity);

	Assertions.assertThat(actualRoundDto) //
		.isNotNull()//
		.matches(actual -> actual.getRoundId().equals(expectedRoundId))//
		.matches(actual -> actual.getPlayerOneChoice().equals(expectedPlayerOneChoice))//
		.matches(actual -> actual.getPlayerTwoChoice().equals(expectedPlayerTwoChoice))//
		.matches(actual -> actual.getResult().equals(expectedRoundResult))//

	;
    }

    @Test
    void testMapRoundEntityToRoundDtoNull() {
	final RoundEntity expectedRoundEntity = null;

	final RoundDto actualRoundDto = this.persistenceMapper.mapRoundEntityToRoundDto(expectedRoundEntity);

	Assertions.assertThat(actualRoundDto).isNull();
    }

    @Test
    void testMapPlayerDtoToPlayerEntity() {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().userName(EXPECTED_USERNAME).build();

	final PlayerEntity actualPlayerEntity = this.persistenceMapper.mapPlayerDtoToPlayerEntity(expectedPlayerDto);

	Assertions.assertThat(actualPlayerEntity) //
		.isNotNull()//
		.matches(actual -> actual.getUserName().equals(EXPECTED_USERNAME));
    }

    @Test
    void testMapPlayerDtoToPlayerEntityNull() {
	final PlayerDto expectedPlayerDto = null;

	final PlayerEntity actualPlayerEntity = this.persistenceMapper.mapPlayerDtoToPlayerEntity(expectedPlayerDto);

	Assertions.assertThat(actualPlayerEntity) //
		.isNull();
    }

    @Test
    void testMapRoundDtoToRoundEntity() {
	final UUID expectedRoundId = UUID.fromString("fc42dbe7-f4a7-44f8-8397-1e7ddd3ab0ba");
	final TypeEnum expectedPlayerOneChoice = TypeEnum.PAPER;
	final TypeEnum expectedPlayerTwoChoice = TypeEnum.ROCK;
	final RoundResultEnum expectedRoundResult = RoundResultEnum.PLAYER1_WINS;

	final RoundDto expectedRoundDto = RoundDto.builder()//
		.roundId(expectedRoundId)//
		.playerOneChoice(expectedPlayerOneChoice)//
		.playerTwoChoice(expectedPlayerTwoChoice)//
		.result(expectedRoundResult)//
		.build();

	final RoundEntity actualRoundEntity = this.persistenceMapper.mapRoundDtoToRoundEntity(expectedRoundDto);

	Assertions.assertThat(actualRoundEntity) //
		.isNotNull()//
		.matches(actual -> actual.getRoundId().equals(UUID.fromString("fc42dbe7-f4a7-44f8-8397-1e7ddd3ab0ba")))//
		.matches(actual -> actual.getPlayerOneChoice().equals(expectedPlayerOneChoice))//
		.matches(actual -> actual.getPlayerTwoChoice().equals(expectedPlayerTwoChoice))//
		.matches(actual -> actual.getResult().equals(expectedRoundResult))//
	;
    }

}

package com.ciklum.alvarez.persistence.pojo;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.common.pojo.PlayerSingletonBean;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.model.entity.PlayerEntity;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class PlayerPersistenceImplTest {

    @InjectMocks
    private PlayerPersistenceImpl playerPersistenceImpl;

    @Mock
    private PlayerSingletonBean playerSingleton;

    @Mock
    private PersistenceMapper persistenceMapper;

    private static final String EXPECTED_USERNAME = "email@test.com";

    @Test
    void testCreatePlayer() throws GameWsApiException {
	final PlayerDto expectedPlayerDto = PlayerDto.builder().userName(EXPECTED_USERNAME).build();

	this.playerPersistenceImpl.createPlayer(expectedPlayerDto);

	Mockito.verify(this.playerSingleton, Mockito.times(1)).insert(BDDMockito.any());
    }

    @Test
    void testFindPlayerById() throws GameWsApiException {
	final Optional<PlayerEntity> expectedPlayerEntity = Optional.of(PlayerEntity.builder().build());
	final PlayerDto expectedPlayerDto = PlayerDto.builder().build();

	BDDMockito.given(this.playerSingleton.get(BDDMockito.any())).willReturn(expectedPlayerEntity);
	BDDMockito.given(this.persistenceMapper.mapPlayerEntityToPlayerDto(BDDMockito.any())).willReturn(expectedPlayerDto);

	this.playerPersistenceImpl.findPlayerById(EXPECTED_USERNAME);

	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapPlayerEntityToPlayerDto(BDDMockito.any());
    }

    @Test
    void testFindAllPlayers() throws GameWsApiException {
	final PlayerEntity expectedPlayerEntity = PlayerEntity.builder().userName(EXPECTED_USERNAME).build();
	final List<PlayerEntity> expectedPlayerEntityList = ImmutableList.of(expectedPlayerEntity);

	BDDMockito.given(this.playerSingleton.getValues()).willReturn(expectedPlayerEntityList);

	this.playerPersistenceImpl.findAllPlayers();

	Mockito.verify(this.persistenceMapper, Mockito.times(1)).mapPlayerEntityToPlayerDto(BDDMockito.any());
    }

}

package com.ciklum.alvarez.service.pojo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ciklum.alvarez.common.pojo.GameWsApiException;
import com.ciklum.alvarez.domain.pojo.GameDomainImpl;
import com.ciklum.alvarez.domain.pojo.dto.GameDto;
import com.ciklum.alvarez.domain.pojo.dto.GameTotalInfoDto;
import com.ciklum.alvarez.domain.pojo.dto.PlayerDto;
import com.ciklum.alvarez.service.pojo.dto.GameResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.GameTotalInfoResponseJsonDto;
import com.ciklum.alvarez.service.pojo.dto.PlayRequestJsonDto;
import com.google.common.collect.ImmutableList;

@ExtendWith(MockitoExtension.class)
class GameServiceRestImplTest {

    @InjectMocks
    private GameServiceRestImpl gameServiceRestImpl;

    @Mock
    private GameDomainImpl gameDomain;

    private static final String EXPECTED_GAME_ID = "0000-1111-2222";

    @Test
    void testGetGameTotalInfo() {
	final GameTotalInfoDto gameTotalInfoDto = GameTotalInfoDto.builder()//
		.build();

	BDDMockito.given(this.gameDomain.findGameTotalInfo()).willReturn(gameTotalInfoDto);

	final ResponseEntity<GameTotalInfoResponseJsonDto> response = this.gameServiceRestImpl.getGameTotalInfo();

	Assertions.assertThat(response) //
		.isNotNull()//
		.matches(res -> res.getStatusCode() == HttpStatus.OK);
    }

    @Test
    void testGetGame() throws GameWsApiException {
	final GameDto expectedGameDto = this.buildExpectedGameDto();

	BDDMockito.given(this.gameDomain.findGameById(EXPECTED_GAME_ID)).willReturn(Optional.of(expectedGameDto));

	final ResponseEntity<GameResponseJsonDto> response = this.gameServiceRestImpl.getGame(EXPECTED_GAME_ID);

	Assertions.assertThat(response) //
		.isNotNull()//
		.matches(res -> res.getStatusCode() == HttpStatus.OK);
    }

    @Test
    void testGetGames() throws GameWsApiException {
	BDDMockito.given(this.gameDomain.findAllGames()).willReturn(ImmutableList.of(this.buildExpectedGameDto()));

	final ResponseEntity<List<GameResponseJsonDto>> response = this.gameServiceRestImpl.getGames();

	Assertions.assertThat(response) //
		.isNotNull()//
		.matches(res -> res.getStatusCode() == HttpStatus.OK);
    }

    @Test
    void testPlay() throws GameWsApiException {
	final PlayRequestJsonDto expectedPlayRequestJsonDto = PlayRequestJsonDto.builder().build();
	final GameDto expectedGameDto = this.buildExpectedGameDto();

	BDDMockito.given(this.gameDomain.play(BDDMockito.any())).willReturn(expectedGameDto);

	final ResponseEntity<GameResponseJsonDto> response = this.gameServiceRestImpl.play(expectedPlayRequestJsonDto);

	Assertions.assertThat(response) //
		.isNotNull()//
		.matches(res -> res.getStatusCode() == HttpStatus.OK);
    }

    private GameDto buildExpectedGameDto() {
	return GameDto.builder()//
		.count(1L)//
		.createdAt(LocalDateTime.now())//
		.gameId(UUID.randomUUID())//
		.playerOne(PlayerDto.builder().build())//
		.playerTwo(PlayerDto.builder().build())//
		.rounds(new ArrayList<>())//
		.build();
    }

}
